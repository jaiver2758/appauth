import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-protegidas',
  templateUrl: './protegidas.component.html',
  styles: []
})
export class ProtegidasComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
    this.auth.userProfile$.subscribe(perfil => {
      console.log(perfil);
    })
  }

}
